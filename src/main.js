import axios from "axios";

document.addEventListener("DOMContentLoaded", function (event) {
  var ele = document.getElementById("myBtn");
  ele.onclick = function () {
    var customText = document.getElementById("myInput").value;

    var payload = { voice: "Hans" };
    const headers = {};

    if (customText != "Some text..") {
      payload = { ...payload, text: customText };
    }

    axios
      .post(
        "https://4xx8ya2nj1.execute-api.eu-west-1.amazonaws.com/prod/",
        payload,
        headers
      )
      .then((response) => {
        document.getElementById("response").innerHTML =
          "<p>" + response.data.text + "</p>";
        var audio = new Audio(response.data.url);
        audio.play();
      })
      .catch((error) => {
        console.log(error);
      });
  };
});
